import React from 'react';
import './Header.css';

export default function Header(props) {
  return (
    <div className="header-Container">
      <img className='header-image' src={props.headerImage} />
      <div className='header-content'>
        <h2 className="header-title">Team Collaboration</h2>
        <p className="header-body">
            Find ways to build teams, engage growth and deliver succesful solutions
            with our tool kit below. Know wehre and how to take your team to the max
            with these tips and strategies.
        </p>
    </div>
    </div>
  );
}
