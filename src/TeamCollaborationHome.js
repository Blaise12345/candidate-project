import React, { Component } from 'react';
import './TeamCollaborationHome.css';

import Header from './Header';
import Filters from './Filters';
import MenuItemCard from './MenuItemCard';

import dataSetJson from './data-set';

export default class TeamCollaborationHome extends Component {
  constructor(props) {
    super(props);

    this._applyFilter = this._applyFilter.bind(this);

    // if no data or improper, pass empty array. Could also use typescript to enforce data types
    const dataSet = (Array.isArray(dataSetJson) && dataSetJson) || [];

    const menuItemCardListMap = {};
    let headerImage = '';

    for (let data of dataSet) {
      if (data.category) {
        if (!menuItemCardListMap[data.category]) {
          menuItemCardListMap[data.category] = [];
        }
        if (!headerImage) {
          headerImage = data.image;
        }
        menuItemCardListMap[data.category].unshift(data);
      }
    }

    let categoryOptions = Object.keys(menuItemCardListMap);
    categoryOptions.unshift('Any');

    this.state = {
      menuItemCardListAll: dataSet,
      menuItemCardListFiltered: dataSet,
      menuItemCardListMap,
      categoryOptions,
      headerImage
    };
  }

  _applyFilter({ selectedCategory }) {
    const menuItemCardListFiltered =
      (selectedCategory === 'Any' && this.state.menuItemCardListAll) ||
      this.state.menuItemCardListMap[selectedCategory] ||
      [];

    this.setState({
      menuItemCardListFiltered
    });
  }

  render() {
    return (
      <div className="Container">
        <Header headerImage={this.state.headerImage} />
        <HorizontalLine />
        <Filters
          categoryOptions={this.state.categoryOptions}
          _applyFilter={this._applyFilter}
        />
        <div className="cardList-container">
          {(this.state.menuItemCardListFiltered || []).map(
            (dataCard, index) => (
              // temporarily setting the key to something unique. Using a library to handle would be better
              <MenuItemCard key={new Date().getTime() + index} {...dataCard} />
            )
          )}
        </div>
      </div>
    );
  }
}

function HorizontalLine() {
  const horizontalLineContainer = {
    padding: '20px',
    width: '100%',
    display: 'flex',
    justifyContent: 'center'
  };

  const horizontalLine = {
    background: '#0052CC',
    height: '5px',
    width: '200px'
  };
  return (
    <div style={horizontalLineContainer}>
      <div style={horizontalLine} />
    </div>
  );
}
