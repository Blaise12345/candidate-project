import React, { Component } from 'react';
import './Filters.css';
import { HTMLSelect } from '@blueprintjs/core';

export default class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: this.props.categoryOptions[0]
    };
  }

  selectOnchange(e) {
    const value = e.currentTarget.value;
    this.setState({
      selectedCategory: value
    });
    this.props._applyFilter({ selectedCategory: value });
  }

  render() {
    return (
      <div className="filter-container">
        <div className="filter-subtitle">{`filter plays by:`}</div>
        <div className="filter-category-label">{`Category`}</div>
        <div className='filter-dropdown-container'>
          <HTMLSelect
            fill={true}
            options={this.props.categoryOptions}
            onChange={e => this.selectOnchange(e)}
            value={this.state.selectedCategory}
          />
        </div>
      </div>
    );
  }
}
