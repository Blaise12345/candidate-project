import React, { Component } from 'react';
import './MenuItemCard.css'

export default class MenuItemCard extends Component {
  constructor(props) {
    super(props);

    console.log(props.image);

    const imageNameFinder = props.image && props.image.match(/[\w-]+.png/g); 
    const imageName = imageNameFinder && imageNameFinder[0]

    this.state = {
      imageName,
    }
  }

  render() {
    return (
    <div className="card-container">
      <img className='card-image' src={this.props.image} alt={this.state.imageName} />
      <div className='card-title'>{this.props.title}</div>
      <div className='card-description'>{this.props.description}</div>
      <a href={this.props.link} className='card-link'>{`Learn more`}</a>
    </div>
    )
  }
}
