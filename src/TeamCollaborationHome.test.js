import React from 'react';
import ReactDOM from 'react-dom';
import TeamCollaborationHome from './TeamCollaborationHome';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TeamCollaborationHome />, div);
  ReactDOM.unmountComponentAtNode(div);
});
