
## Instructions

Created from the basic create react project template.

1. Download source
2. yarn install
3. npm start (this should open up browser window at http://localhost:3000/ )

---

## Improvements

1. CSS adjustments with spacing, padding, and alignment.
2. Use a different 3rd party react component for the category dropdown - not a big fan of the select styling
3. Remove unecessary/empty CSS classes.
4. Add a {NavigationBar} component for Atlassian Logo and login.
5. Change data handling a bit. Currently, the Json data is read in as file, but this should be added in the componentDidMount as that's typically where a restApi call would sit to get data.
6. Filters component could be built using Hooks.
7. MenuCardItem can be a functional react component instead of a class. For simplicity.
8. Move away from using CSS Grid display to Flex because 'grid-template-columns' isn't supported by IE. Alternatively, there is a IE polyfill which could fix that.
9. Install webpack for better building bundling.
10. Minify code for production
11. Reorganize files and folders: src: components, style, tests
12. Expand jest tests.